use std::path::Path;

use rusty_ytdl::{Video, VideoError, VideoOptions};

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let url = "https://www.youtube.com/watch?v=RPa3_AD1_Vs";
    let path = Path::new("test.mp4");

    download_youtube(url, path).await.unwrap();
}

/// Download youtube video to disk
async fn download_youtube<P: AsRef<Path>>(url: &str, path: P) -> Result<(), VideoError> {
    let video = Video::new_with_options(
        url,
        VideoOptions {
            quality: rusty_ytdl::VideoQuality::Highest,
            filter: rusty_ytdl::VideoSearchOptions::VideoAudio,
            ..Default::default()
        },
    )?;

    video.download(path).await
}

#[cfg(test)]
mod test {
    use hex_literal::hex;
    use sha2::{Digest, Sha256};

    use crate::download_youtube;
    use std::{env::temp_dir, fs::File, io::copy};

    #[tokio::test]
    async fn download_youtube_me_at_the_zoo() {
        let url = "https://www.youtube.com/watch?v=jNQXAC9IVRw";
        let path = temp_dir().join("me-at-the-zoo.mp4");

        download_youtube(url, &path)
            .await
            .expect("Could not download video");

        let mut file = File::open(&path).unwrap();
        let mut sha256 = Sha256::new();
        copy(&mut file, &mut sha256).unwrap();
        assert_eq!(
            sha256.finalize()[..],
            hex!("9127aaafccef6f02fce6812bc9c89e1e4026832cf133492481952cc4b94cb595")[..]
        );
    }
}
