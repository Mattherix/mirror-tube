# Mirror tube

Une simple interface web utilisé pour télécharger et diffuser des vidéos de 
Youtube hors ligne :).

Je pense utilisé Postgres, Redis ou les deux afin de stocker les données 
utilisateurs, la file d'attente pour les tâches de téléchargements et peut-être
pour de la mise en cache.

Utilisons Axum et sqlx !

https://github.com/Mithronn/rusty_ytdl/tree/main?tab=readme-ov-file
